let time = 0;
let currentTime = 0;
let milliseconds=0;
let minutes=0;
let seconds=0;
let hours=0;
function startPause(){
    if(currentTime === 0){
        currentTime = 1;
        increment();
        document.getElementById("start").innerHTML = "Pause";
    }
    else{
        currentTime = 0;
        document.getElementById("start").innerHTML = "Resume";
    }
}

function reset(){
    currentTime = 0;
    time = 0;
    minutes=0;
    milliseconds=0;
    hours=0;
    document.getElementById("start").innerHTML = "Start";
    document.getElementById("output").innerHTML = "00:00:00:0";
}

function increment(){
    if(currentTime === 1){
        setTimeout (() => {
            minutes = Math.floor(time/10/60);
            seconds = Math.floor(time/10 % 60);
            hours = Math.floor(time/10/60/60);
            milliseconds = time % 10;
            time++;
            if(minutes < 10){
                minutes = "0" + minutes;
            }
            if(seconds < 10){
                seconds = "0" + seconds;
            }
            document.getElementById("output").innerHTML = "0" +hours + ":" + minutes + ":" + seconds + ":" + milliseconds ;
            increment();
        },100)
    }
}
