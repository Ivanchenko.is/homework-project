const creative = {
  name: 'CreativeCloud',
  status: 'small',
  department: {
    it: {
      designer: 'Antony',
      tester: 'Clarens',
      content: 'Asitov',
      programist: {
        online: 'John',
        offline: 'Gogi',
       }
     },
    finance: {
      analyst: 'Java',
      economist: 'We dont have economist',
    }
  }
};

function clone(obj) {
  let cloneObj = {}
  for( let key in obj) {
    if(typeof obj[key] == 'object'){
      cloneObj[key] = clone(obj[key])
    } else{
      cloneObj[key] = obj[key]
    }
  };
  return cloneObj
};

console.log(creative);
